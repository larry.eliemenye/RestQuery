/**
 * Created by tthlex on 04/01/16.
 */

var PRE_REQUEST_METHODS = {};
PRE_REQUEST_METHODS.entityPath = function(){
    return this;
};

PRE_REQUEST_METHODS.fetch = function(){
    return this;
};

PRE_REQUEST_METHODS.fetchById = function(){
    return this;
};

PRE_REQUEST_METHODS.select = function(){
    return this;
};

module.exports = PRE_REQUEST_METHODS;