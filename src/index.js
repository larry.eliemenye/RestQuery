/**
 *
 * By Larry Eliemenye
 *
 * */


var _ =             require('lodash');
var util =          require('util');
var qs =   require('querystring');
var request =       require('request');


//methods to inherit
var METHOD_OBJECTS = [
    require('./prerequest'),
    require('./manipulators'),
    require('./postrequest'),
    require('./transforms'),
    require('./aggregators')
];

//Main Ctor
function RestQuery(options) {
    this.host = options.host || "";
    this.default_headers = options.default_headers || "";
}

//Extend original prototype with all methods
METHOD_OBJECTS.forEach(function (mObject) {
    _.merge(RestQuery.prototype, mObject);
});

module.exports = RestQuery;



