# RestQuery - A simple library for querying Restful APIs

## Basic Example
```js
var RestQuery = require('rest-query');

//issues a get request to http://api.beconaize.com/data/destinations using id=123 as query passing in the default_headers for every instance of this call
var RQ = new RestQuery({host:'http://api.beconaize.com', default_headers:{
    "Authorization": "Basic 34gurgy34uygirehggd",
    "x-token-access": "e56a7b36b7a8e63v876e08d97f86a97e63b35e"
}})
  .entity('/data/destinations')
  .query({id:123}, function(err, data){
    if(err) throw error
    res.json(data);
  });
```

## Streaming responses directly to client

```js
//streams response directly to client, Useful for straightforward proxying without applying any intermidiate transfromation logic
RQ.entity('/data/destinations').pipe(res);
```

## Transforming Responses
You can transform responses return from queries like so.

### Map and Skip
```js

//makes a call to the endpoint, return results and applies a map to the result set, skipping a few results
RQ.entity('/data/destinations')
  .skip
  .map(fn)
```

### limit and filter
```js

//makes a call to the endpoint, limits the result to the first 20 and applies a filter to the resulting dataset
RQ
  .entity('/data/destinations')
  .limit(20)
  .filter(fn)
```

## TODO
1. Add a promise API
2. add Method to override base headers